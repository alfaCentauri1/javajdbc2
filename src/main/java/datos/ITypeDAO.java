package datos;

import java.util.List;

public interface ITypeDAO {
    int insertar(Object nuevo);
    List listar();
    int actualizar(Object objeto);
    int eliminar(int id);
    Object buscarPorId(int id);
    Object buscar(Object objeto);
}
