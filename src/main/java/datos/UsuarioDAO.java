package datos;

import domain.Usuario;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static datos.Conexion.getConnection;

public class UsuarioDAO extends DAO implements ITransactionalDAO{
    private static final String SQL_INSERT = "INSERT INTO usuario (usuario, password) VALUES (?,?);";
    private static final String SQL_SELECT = "SELECT id, usuario, password FROM usuario;";
    private static final String SQL_UPDATE = "UPDATE usuario SET nombre = ?, password = ? WHERE id = ?";
    private static final String SQL_DELETE = "DELETE FROM usuario WHERE id=?;";
    private static final String SQL_SELECT_FIND_BY_ID = "SELECT id, nombre, password FROM usuario WHERE id=? ORDER BY id ASC;";
    private Usuario usuario;    
    private List<Usuario> usuarios;

    /**
     *
     * @param conexionTransactional Type Connection.
     */
    public void UsuarioDAO(Connection conexionTransactional){
        if (conexionTransactional != null) {
            this.conexionTransactional = conexionTransactional;
        }
        else{
            try {
                this.conexionTransactional = getConnection();
            } catch (SQLException exception) {
                System.out.println("Error al establecer la conexión: " + exception.getMessage());
            }
        }
    }
    
    @Override
    public int insertar(Object nuevo) {
        int registros = 0;
        usuario = (Usuario) nuevo;
        try{
            conexionTransactional = getConnection();
            preparedStatement = conexionTransactional.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1, usuario.getUsuario());
            preparedStatement.setString(2, usuario.getPassword());
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al insertar el registro: " + usuario + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
            return registros;
        }
    }

    @Override
    public List listar() {
        usuarios = new ArrayList<>();
        try{
            conexionTransactional = getConnection();
            preparedStatement = conexionTransactional.prepareStatement(SQL_SELECT);
            resultado = preparedStatement.executeQuery();
            while (resultado.next()){
                usuario = new Usuario();
                usuario.setId(resultado.getInt("id"));
                usuario.setUsuario(resultado.getString("usuario"));
                usuario.setPassword(resultado.getString("password"));
                usuarios.add(usuario);
            }
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        finally {
            this.closeConections();
        }
        return usuarios;
    }

    @Override
    public int actualizar(Object objeto) {
        int registros = 0;
        usuario = (Usuario) objeto;
        try{
            conexionTransactional = getConnection();
            preparedStatement = conexionTransactional.prepareStatement(SQL_UPDATE);
            preparedStatement.setString(1, usuario.getUsuario());
            preparedStatement.setString(2, usuario.getPassword());
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al actualizar el registro: " + usuario + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
            return registros;
        }
    }

    @Override
    public int eliminar(int id) {
        int registros = 0;
        try{
            conexionTransactional = getConnection();
            preparedStatement = conexionTransactional.prepareStatement(SQL_DELETE);
            preparedStatement.setInt(1, id);
            registros = preparedStatement.executeUpdate();
        }catch (Exception exception){
            System.out.println("Error al actualizar el registro de indice: " + id + "\n" + exception.getMessage());
        }
        finally {
            try {
                Conexion.close(preparedStatement);
            } catch (SQLException exception) {
                System.out.println("Error: " + exception.getMessage());
            }
            return registros;
        }
    }

    @Override
    public Object buscarPorId(int id) {
        usuario = new Usuario();
        try{
            conexionTransactional = getConnection();
            preparedStatement = conexionTransactional.prepareStatement(SQL_SELECT_FIND_BY_ID);
            preparedStatement.setInt(1, id);
            this.getFielsTableToUsuario();
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        finally {
            this.closeConections();
        }
        return usuario;
    }

    private void getFielsTableToUsuario() throws Exception{
        resultado = preparedStatement.executeQuery();
        while (resultado.next()){
            usuario.setId(resultado.getInt("id"));
            usuario.setUsuario(resultado.getString("usuario"));
            usuario.setPassword(resultado.getString("password"));
        }
    }

    @Override
    public Object buscar(Object objeto) {
        usuario = (Usuario) objeto;
        try{
            conexionTransactional = getConnection();
            preparedStatement = conexionTransactional.prepareStatement(SQL_SELECT_FIND_BY_ID);
            preparedStatement.setInt(1, usuario.getId());
            this.getFielsTableToUsuario();
        }catch(Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
        finally {
            this.closeConections();
        }
        return usuario;
    }

    private void closeConections(){
        try{
            Conexion.close(resultado);
            Conexion.close(preparedStatement);
        }catch (Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
    }

    @Override
    public void closeConection() {
        try{
            Conexion.close(conexionTransactional);
        }catch (Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }
    }
}
