create database javaTest;
use javaTest;
create table usuario(
    id int autoincrement primary key,
    usuario varchar(50) not null,
    password varchar(50) not null,
);

create table persona(
    id_persona int autoincrement primary key,
    nombre varchar(45) not null,
    apellido varchar(45) not null,
    email varchar(45) not null,
    nombre long default 0,
);